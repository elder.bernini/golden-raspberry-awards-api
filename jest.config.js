module.exports = {
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: ["<rootDir>/src/modules/**/services/*.ts"],
  coverageDirectory: "coverage",
  coverageReporters: ["text-summary", "json", "text", "lcov", "clover"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "^@modules/(.*)$": "<rootDir>/src/modules/$1", // Adicionando mapeamento de módulo para @modules
    "^@config/(.*)$": "<rootDir>/src/config/$1", // Mapeamento para @config
    "^@shared/(.*)$": "<rootDir>/src/shared/$1", // Mapeamento para @shared
  },
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  preset: "ts-jest",
  setupFiles: ["dotenv/config"],
  testMatch: ["**/*.spec.ts"],
};
