# Golden Raspberry Awards API

Projeto de teste desenvolvido em [Node.js](https://nodejs.org/en) criado com intuito de aprendizado.

## Instalação

Realize a instalação do [Node](https://nodejs.org/en/download) de acordo com o sistema operacional. (No desenvolvimento foi utilizada a versao 16.20, versões mais recentes deverão funcionar corretamente)

Use o [git](https://git-scm.com/) para clonar o repositório

```shell
git clone https://gitlab.com/elder.bernini/golden-raspberry-awards-api.git
```

Realziar a instalação dos pacotes atraves do gerenciador [Yarn](https://yarnpkg.com/) para instalar as dependências (NPM também pode ser utilizado, porém os comandos descritos utilizarão o Yarn)<br/>
Caso não possua o Yarn [seguir o processo de instalação](https://classic.yarnpkg.com/lang/en/docs/install) <br/>
Executar o comando abaixo na pasta raiz do projeto

```bash
yarn install
```

## Utilização

Execute o comando no terminal com o [YARN](https://yarnpkg.com/) para iniciar o servidor.

```bash
$ yarn dev
```

Ao iniciar o servidor, a carga inicial de dados será realizada pela leitura do arquivo `/assets/movielist.csv` presente no projeto, os dados serão lidos e inseridos no banco de dados.<br/><br/>
Para fins didáticos o banco utilizado é o [SQLite](https://sqlitebrowser.org/) que será executado em memória, portanto ao reiniciar o servidor o mesmo será resetado. <br/><br/>
O servidor será iniciado na porta definida na propriedade `PORT` do arquivo `.env` presente na raiz do projeto (padrão 3001).

## Docs

```bash
POST /api/movies/load-file
Realiza o load do arquivo `/assets/movielist.csv` manualmente (não necessário quando realizar a carga ao iniciar o servidor)

```

<details><summary>Ver json retorno</summary>

```jsoniq
XXX Movies has been created successfully
```

</details>

```bash
GET /api/movies/producers/min-max-interval
Busca os produtores com maior intervalo entre dois prêmios consecutivos, e os que obteviram dois prêmios mais rápido
```

<details><summary>Ver json retorno</summary>

```jsoniq
{
  "min": [
    {
      "producer": "Producer 1",
      "interval": 6,
      "previousWin": 1984,
      "followingWin": 1990
    },
    {
      "producer": "Producer 2",
      "interval": 6,
      "previousWin": 2002,
      "followingWin": 2008
    }
  ],
  "max": [
    {
      "producer": "Producer 3",
      "interval": 20,
      "previousWin": 2002,
      "followingWin": 2022
    }
  ]
}
```

</details>
<br/>
