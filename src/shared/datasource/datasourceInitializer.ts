/* eslint-disable no-console */
import { container } from "tsyringe";
import TypeORMDataSource from "./sqlite/typeOrmDatasource";

class DatasourceInitializer {
  async init(): Promise<void> {
    console.info("\x1b[32m%s\x1b[0m", "Initializing database...");

    const dataSource =
      container.resolve<TypeORMDataSource>("TypeORMDataSource");

    await dataSource.initialize();
  }
}

export default new DatasourceInitializer();
