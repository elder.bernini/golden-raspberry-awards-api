/* eslint-disable @typescript-eslint/ban-types */
import Movie from "@modules/movies/models/movie.model";
import Producer from "@modules/movies/models/producer.model";
import Studio from "@modules/movies/models/studio.model";
import { EntitySchema, MixedList } from "typeorm";

export default function getEntities(): MixedList<
  Function | string | EntitySchema
> {
  return [Producer, Studio, Movie];
}
