import { DataSource, EntityTarget, ObjectLiteral, Repository } from "typeorm";

export default interface TypeORMDataSource {
  initialize(): Promise<DataSource>;

  getRepository<Entity extends ObjectLiteral>(
    entity: EntityTarget<Entity>
  ): Repository<Entity>;
}
