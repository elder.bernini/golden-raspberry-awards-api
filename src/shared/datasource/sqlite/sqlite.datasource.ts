import "reflect-metadata";
import { DataSource } from "typeorm";
import getEntities from "./entities";

const dropOnInit = () => process.env.DROP_DATABASE_ON_INIT == "TRUE";

const SqliteDataSource = new DataSource({
  type: "sqlite",
  database: ":memory:",
  dropSchema: dropOnInit(),
  synchronize: true,
  entities: getEntities(),
});

export default SqliteDataSource;
