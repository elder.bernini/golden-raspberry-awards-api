import "reflect-metadata";
import { injectable } from "tsyringe";
import { DataSource, EntityTarget, ObjectLiteral, Repository } from "typeorm";
import SqliteDataSource from "./sqlite.datasource";
import TypeORMDataSource from "./typeOrmDatasource";

@injectable()
export default class TypeORMDataSourceImpl implements TypeORMDataSource {
  private datasource: DataSource;

  constructor() {
    this.datasource = SqliteDataSource;
  }

  private getDataSource(): DataSource {
    return this.datasource;
  }

  public initialize(): Promise<DataSource> {
    return this.getDataSource().initialize();
  }

  public getRepository<Entity extends ObjectLiteral>(
    entity: EntityTarget<Entity>
  ): Repository<Entity> {
    return this.getDataSource().getRepository(entity);
  }
}
