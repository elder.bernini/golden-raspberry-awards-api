import "reflect-metadata";

import cors from "cors";
import dotenv from "dotenv";
import express from "express";

import "@config/log/logInitializing";
dotenv.config();

import "@shared/container";
import errorHandler from "@shared/http/middlewares/defaultErrorHandler";
import routes from "@shared/http/routes";

const app = express();

app.use(cors());
app.use(express.json());
app.use("/api", routes);
app.use(errorHandler);

export default app;
