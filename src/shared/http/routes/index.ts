import movieRoutes from "@modules/movies/http/routes/movie.route";
import { Router } from "express";
import homeRouter from "./home";

const routes = Router();

routes.use(homeRouter);
routes.use("/movies", movieRoutes);

export default routes;
