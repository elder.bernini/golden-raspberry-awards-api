import { Request, Response, Router } from "express";

const homeRouter = Router();

homeRouter.get(
  "/",
  (req: Request, res: Response): Response<string> =>
    res.json("We are on line...")
);

export default homeRouter;
