import LogProvider from "@shared/container/providers/logProvider/models/log.provider";
import ApiError from "@shared/erros/apiError";
import { NextFunction, Request, Response } from "express";
import { container } from "tsyringe";

interface ErrorResponse {
  message: string;
  code: string;
}

const logger = container.resolve<LogProvider>("LogProvider");

export default function defaultErrorHandler(
  error: Error,
  _: Request,
  response: Response,
  __: NextFunction
): Response {
  if (error instanceof ApiError) {
    const errorRes: ErrorResponse = {
      message: error.message,
      code: error.statusCode.toString(),
    };

    return response.status(error.statusCode).json(errorRes);
  }

  if (process.env.NODE_ENV !== "test") {
    logger.error("Internal error", error);
  }

  const defaultRes: ErrorResponse = {
    message: `Internal error: ${error.message}`,
    code: "500",
  };

  return response.status(parseInt(defaultRes.code)).json(defaultRes);
}
