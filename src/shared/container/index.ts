import { container } from "tsyringe";

import MovieRepositoryImpl from "@modules/movies/datasource/movie.repository.impl";
import ProducerRepositoryImpl from "@modules/movies/datasource/producer.repository.impl";
import StudioRepositoryImpl from "@modules/movies/datasource/studio.repository.impl";
import MovieRepository from "@modules/movies/repositories/movie.repository";
import ProducerRepository from "@modules/movies/repositories/producer.repository";
import StudioRepository from "@modules/movies/repositories/studio.repository";
import GetProducersMinMaxIntervalService from "@modules/movies/services/getProducersMinMaxInterval.service";
import LoadMoviesService from "@modules/movies/services/loadMovies.service";
import TypeORMDataSource from "@shared/datasource/sqlite/typeOrmDatasource";
import TypeORMDataSourceImpl from "@shared/datasource/sqlite/typeOrmDatasource.impl";
import "./providers";

// DataSource
container.registerSingleton<TypeORMDataSource>(
  "TypeORMDataSource",
  TypeORMDataSourceImpl
);

container.registerSingleton<MovieRepository>(
  "MovieRepository",
  MovieRepositoryImpl
);
container.registerSingleton<ProducerRepository>(
  "ProducerRepository",
  ProducerRepositoryImpl
);
container.registerSingleton<StudioRepository>(
  "StudioRepository",
  StudioRepositoryImpl
);

container.register<LoadMoviesService>("LoadMoviesService", {
  useFactory: (c) => c.resolve(LoadMoviesService),
});

container.registerSingleton<GetProducersMinMaxIntervalService>(
  "GetProducersMinMaxIntervalService",
  GetProducersMinMaxIntervalService
);
