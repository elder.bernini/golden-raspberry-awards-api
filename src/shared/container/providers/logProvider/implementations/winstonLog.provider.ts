/* eslint-disable @typescript-eslint/no-explicit-any */
import dayjs from "dayjs";
import * as os from "os";
import { createLogger, format, Logger, transports } from "winston";
import LogProvider from "../models/log.provider";

export default class WinstonLogProvider implements LogProvider {
  private logger: Logger;

  constructor() {
    this.logger = createLogger({
      level: this.levelLog(),
      format: format.printf((error) => {
        const cabecalho = `[${dayjs().format(
          "YYYY-MM-DD HH:mm:ss"
        )}] - ${error.level.toUpperCase()} - ${error.message}`;

        const keys = Object.keys(error).filter((key) => key.match(/\d/));
        if (!keys.length) {
          return cabecalho;
        }

        const values = keys.map((k) => error[k]);

        return `${cabecalho} ${values.reduce((acc, curr) => {
          if (curr.stack) {
            return `\n${acc} ${curr.stack}\n`;
          }
          if (typeof curr === "object") {
            return `${acc} - ${JSON.stringify(curr)}`;
          }
          return `${acc} - ${curr}`;
        }, "")} \n`;
      }),
    });

    this.addTransports();
  }

  private levelLog(): string {
    if (process.env.NODE_ENV === "prod") {
      return "error";
    }

    if (process.env.NODE_ENV === "hml") {
      return "info";
    }
    return "debug";
  }

  private fileLog(): string {
    return `all-${dayjs().format("YYYY-MM-DD")}.log`;
  }

  private pathFile(): string {
    // TODO pegar o nome do projeto do env, e ajustar no docker compose
    const name = "golden-raspberry-awards-api";

    if (process.env.NODE_ENV === "dev" && os.platform() === "linux") {
      return `log/${this.fileLog()}`;
    }

    return os.platform() === "win32"
      ? `c:\\temp\\${name}\\${this.fileLog()}`
      : `/var/log/${name}/${this.fileLog()}`;
  }

  private addTransports(): void {
    if (process.env.NODE_ENV === "dev") {
      this.logger.add(new transports.Console());
    } else {
      this.logger.add(
        new transports.File({
          filename: this.pathFile(),
          level: "debug",
          handleExceptions: true,
          maxsize: 10485760,
          maxFiles: 20,
        })
      );
    }
  }

  public info(mensagem: string, ...meta: any[]): void {
    this.logger.info(mensagem, meta);
  }

  public error(mensagem: string, ...meta: any[]): void {
    this.logger.error(mensagem, meta);
  }

  public warn(mensagem: string, ...meta: any[]): void {
    this.logger.warn(mensagem, meta);
  }
}
