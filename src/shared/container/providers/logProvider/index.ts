import { container } from 'tsyringe';
import WinstonLogProvider from './implementations/winstonLog.provider';
import LogProvider from './models/log.provider';

container.register<LogProvider>('LogProvider', WinstonLogProvider);
