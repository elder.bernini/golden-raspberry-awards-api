/* eslint-disable @typescript-eslint/no-explicit-any */
export default interface LogProvider {
  info(message: string, ...meta: any[]): void;
  error(message: string, ...meta: any[]): void;
  warn(message: string, ...meta: any[]): void;
}
