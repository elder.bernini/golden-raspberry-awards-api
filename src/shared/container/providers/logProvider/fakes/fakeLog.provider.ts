/* eslint-disable @typescript-eslint/no-explicit-any */
import LogProvider from '../models/log.provider';

export default class FakeLogProvider implements LogProvider {
  private logs: any[] = [];

  info(message: string, ...meta: any[]): void {
    this.logs.push(message);
  }

  error(message: string, ...meta: any[]): void {
    this.logs.push(message);
  }

  warn(message: string, ...meta: any[]): void {
    this.logs.push(message);
  }
}
