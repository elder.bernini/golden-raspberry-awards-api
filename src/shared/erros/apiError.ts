interface NamedParameters {
  message: string;
  statusCode?: number;
}

export default class ApiError {
  public readonly message: string;

  public readonly statusCode: number;

  constructor({ message, statusCode = 500 }: NamedParameters) {
    this.message = message;
    this.statusCode = statusCode;
  }
}
