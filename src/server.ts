import "reflect-metadata";

import app from "@shared/app";
import DatasourceInitializer from "@shared/datasource/datasourceInitializer";

import loadMoviesOnInitService from "@modules/movies/services/loadMoviesOnInit.service";

const port = Number(process.env.PORT) || 3333;

DatasourceInitializer.init()
  .then((_) => {
    console.info("\x1b[32m%s\x1b[0m", "Server is starting...");

    app.listen(port, () => {
      console.info("\x1b[32m%s\x1b[0m", `Server started on port ${port}.`);

      loadMoviesOnInitService.execute();
    });
  })
  .catch((e) => console.error(e));
