import CreateProducerDto from "../dto/createProducer.dto";
import Producer from "../models/producer.model";

export default interface ProducerRepository {
  save(data: CreateProducerDto): Promise<Producer>;

  findByName(name: string): Promise<Producer | null>;

  findWinnersMoreThanOnce(): Promise<Producer[]>;
}
