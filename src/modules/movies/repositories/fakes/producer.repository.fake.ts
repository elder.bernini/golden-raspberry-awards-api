import CreateProducerDto from "@modules/movies/dto/createProducer.dto";
import Producer from "@modules/movies/models/producer.model";
import { v4 as uuid_v4 } from "uuid";
import ProducerRepository from "../producer.repository";

export default class ProducerRepositoryFake implements ProducerRepository {
  private itens: Producer[] = [];

  async save(data: CreateProducerDto): Promise<Producer> {
    const item = new Producer();

    Object.assign(item, { id: uuid_v4() }, data);

    this.itens.push(item);

    return item;
  }

  async findByName(name: string): Promise<Producer | null> {
    return this.itens.find((s) => s.name == name) || null;
  }

  findWinnersMoreThanOnce(): Promise<Producer[]> {
    throw new Error("Method not implemented.");
  }
}
