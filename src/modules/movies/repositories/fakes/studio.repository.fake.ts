import CreateStudioDto from "@modules/movies/dto/createStudio.dto";
import Studio from "@modules/movies/models/studio.model";
import { v4 as uuid_v4 } from "uuid";
import StudioRepository from "../studio.repository";

export default class StudioRepositoryFake implements StudioRepository {
  private itens: Studio[] = [];

  async save(data: CreateStudioDto): Promise<Studio> {
    const item = new Studio();

    Object.assign(item, { id: uuid_v4() }, data);

    this.itens.push(item);

    return item;
  }

  async findByName(name: string): Promise<Studio | null> {
    return this.itens.find((s) => s.name == name) || null;
  }
}
