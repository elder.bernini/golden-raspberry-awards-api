import CreateMovieDto from "@modules/movies/dto/createMovie.dto";
import Movie from "@modules/movies/models/movie.model";
import { v4 as uuid_v4 } from "uuid";
import MovieRepository from "../movie.repository";

export default class MovieRepositoryFake implements MovieRepository {
  private itens: Movie[] = [];

  async save(data: CreateMovieDto): Promise<Movie> {
    const item = new Movie();

    Object.assign(item, { id: uuid_v4() }, data);

    this.itens.push(item);

    return item;
  }
}
