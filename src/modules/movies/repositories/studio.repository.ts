import CreateStudioDto from "../dto/createStudio.dto";
import Studio from "../models/studio.model";

export default interface StudioRepository {
  save(data: CreateStudioDto): Promise<Studio>;

  findByName(name: string): Promise<Studio | null>;
}
