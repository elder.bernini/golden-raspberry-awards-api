import CreateMovieDto from "../dto/createMovie.dto";
import Movie from "../models/movie.model";

export default interface MovieRepository {
  save(data: CreateMovieDto): Promise<Movie>;
}
