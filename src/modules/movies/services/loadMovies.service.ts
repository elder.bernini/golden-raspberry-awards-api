import LogProvider from "@shared/container/providers/logProvider/models/log.provider";
import ApiError from "@shared/erros/apiError";
import csvParser from "csv-parser";
import fs from "fs";
import { inject, injectable } from "tsyringe";
import CreateMovieDto from "../dto/createMovie.dto";
import ExtractMovieFileDto from "../dto/extractMovieFile.dto";
import MovieFileDto from "../dto/movieFile.dto";
import Producer from "../models/producer.model";
import Studio from "../models/studio.model";
import MovieRepository from "../repositories/movie.repository";
import ProducerRepository from "../repositories/producer.repository";
import StudioRepository from "../repositories/studio.repository";

@injectable()
class LoadMoviesService {
  private producersCache: Producer[];
  private studiossCache: Studio[];

  constructor(
    @inject("MovieRepository")
    private movieRepository: MovieRepository,

    @inject("ProducerRepository")
    private producerRepository: ProducerRepository,

    @inject("StudioRepository")
    private sudioRepository: StudioRepository,

    @inject("LogProvider")
    private logger: LogProvider
  ) {
    this.producersCache = [];
    this.studiossCache = [];
  }

  async execute(fileName?: string, separator?: string): Promise<number> {
    try {
      const data = await this.readCsv(fileName, separator);

      if (data == null || data.length == 0) {
        return 0;
      }

      for (const row of data) {
        const producersMovie = await this.getProducersMovie(row.producers);
        const studiosMovie = await this.getStudiosMovie(row.studios);

        const movie: CreateMovieDto = {
          year: row.year,
          title: row.title,
          producers: producersMovie.map((p) => ({ id: p })),
          studios: studiosMovie.map((p) => ({ id: p })),
          win: row.winner,
        };

        await this.movieRepository.save(movie);
      }

      return data.length;
    } catch (error) {
      if (error instanceof Error) {
        throw new ApiError({
          message: `Failure during load movies from file: ${error.message}`,
        });
      }
      throw new ApiError({
        message: "Unknown failure during load movies from file",
      });
    }
  }

  private async getProducersMovie(producersNames: string[]) {
    return Promise.all(
      producersNames.map(async (p) => {
        let producer = this.findProducerByName(p);
        if (producer == null) {
          producer = await this.producerRepository.save({ name: p });
          this.producersCache.push(producer);
        }
        return producer.id;
      })
    );
  }

  private async getStudiosMovie(studiosNames: string[]) {
    return Promise.all(
      studiosNames.map(async (p) => {
        let studio = this.findStudiosByName(p);
        if (studio == null) {
          studio = await this.sudioRepository.save({ name: p });
          this.studiossCache.push(studio);
        }
        return studio.id;
      })
    );
  }

  private findProducerByName(name: string) {
    return this.producersCache.find((p) => p.name == name);
  }

  private findStudiosByName(name: string) {
    return this.studiossCache.find((p) => p.name == name);
  }

  private async readCsv(file: string = "movielist.csv", separator = ";") {
    const filePath = `./assets/${file}`;
    const rows: Array<MovieFileDto> = [];

    await new Promise((resolve) => {
      //tratamento de erro falho, stream nao lança failure quando falha
      fs.createReadStream(filePath)
        .pipe(csvParser({ separator }))
        .on("data", (row: ExtractMovieFileDto) => {
          try {
            rows.push(new MovieFileDto(row));
          } catch (e) {
            this.logger.error("Error reading csv file", e);
          }
        })
        .on("end", () => {
          resolve(true);
        });
    });

    return rows;
  }
}

export default LoadMoviesService;
