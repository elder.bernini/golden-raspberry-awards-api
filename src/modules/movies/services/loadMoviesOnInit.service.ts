import { container } from "tsyringe";
import LoadMoviesService from "./loadMovies.service";

class LoadMoviesOnInitService {
  async execute(): Promise<void> {
    const load = process.env.LOAD_ON_INIT === "TRUE";
    if (load) {
      console.info("\x1b[32m%s\x1b[0m", "Initialize load movies from file");

      const loadService =
        container.resolve<LoadMoviesService>("LoadMoviesService");

      const movies = await loadService.execute();

      console.info(
        "\x1b[32m%s\x1b[0m",
        `${movies} movies has been imported from file`
      );
    }
  }
}

export default new LoadMoviesOnInitService();
