export default interface CreateMovieDto {
  year: number;

  title: string;

  win: boolean;

  studios: { id: string }[];

  producers: { id: string }[];
}
