import ExtractMovieFileDto from "./extractMovieFile.dto";

export default class MovieFileDto {
  constructor(data: ExtractMovieFileDto) {
    this.year = parseInt(data.year);
    this.title = data.title.trim();
    this.studios = data.studios.split(",").map((s) => s.trim());
    this.producers = data.producers.split(",").map((p) => p.trim());
    this.winner = data.winner.toLowerCase() == "yes";
  }

  year: number;

  title: string;

  studios: string[];

  producers: string[];

  winner: boolean;
}
