export default interface ExtractMovieFileDto {
  year: string;

  title: string;

  studios: string;

  producers: string;

  winner: string;
}
