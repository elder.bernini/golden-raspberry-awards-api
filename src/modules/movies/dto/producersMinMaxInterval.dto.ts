export interface ProducersMinMaxIntervalResponseDto {
  min: ProducerMinMaxIntervalDto[];
  max: ProducerMinMaxIntervalDto[];
}

export interface ProducerMinMaxIntervalDto {
  producer: string;
  interval: number;
  previousWin: number;
  followingWin: number;
}

export interface ProducerIntervalDto {
  producer: string;
  interval: MoviesDiffInterval;
}

export interface MoviesDiffInterval {
  min: number;
  max: number;
  minPair: number[];
  maxPair: number[];
}
