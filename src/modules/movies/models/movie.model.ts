import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import Producer from "./producer.model";
import Studio from "./studio.model";

@Entity({ name: "movie" })
export default class Movie {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  year: number;

  @Column({ length: 100 })
  title: string;

  @Column()
  win: boolean;

  @ManyToMany(() => Studio)
  @JoinTable({
    name: "movie-studio",
  })
  studios: Studio[];

  @ManyToMany(() => Producer, (p) => p.movies)
  @JoinTable({
    name: "movie-producer",
  })
  producers: Producer[];
}
