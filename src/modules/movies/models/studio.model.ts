import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "studio" })
export default class Studio {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ length: 100 })
  name: string;
}
