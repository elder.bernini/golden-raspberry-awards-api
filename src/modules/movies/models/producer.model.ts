import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import Movie from "./movie.model";

@Entity({ name: "producer" })
export default class Producer {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ length: 100 })
  name: string;

  @ManyToMany(() => Movie, (movie) => movie.producers)
  movies: Movie[];
}
