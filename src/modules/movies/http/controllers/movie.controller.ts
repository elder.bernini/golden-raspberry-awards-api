import GetProducersMinMaxIntervalService from "@modules/movies/services/getProducersMinMaxInterval.service";
import LoadMoviesService from "@modules/movies/services/loadMovies.service";
import { Request, Response } from "express";
import { container } from "tsyringe";

export default class MovieController {
  async load(req: Request, res: Response): Promise<Response> {
    const loadService = container.resolve(LoadMoviesService);

    const movies = await loadService.execute();

    return res
      .status(201)
      .json(`${movies} Movies has been created successfully`);
  }

  async getProducer(req: Request, res: Response): Promise<Response> {
    const service = container.resolve(GetProducersMinMaxIntervalService);

    const producerRes = await service.execute();

    return res.status(200).json(producerRes);
  }
}
