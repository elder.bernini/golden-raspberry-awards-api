import { Router } from "express";
import MovieController from "../controllers/movie.controller";

const movieRoutes = Router();

const movieController = new MovieController();

movieRoutes.post("/load-file", movieController.load);

movieRoutes.get("/producers/min-max-interval", movieController.getProducer);

export default movieRoutes;
