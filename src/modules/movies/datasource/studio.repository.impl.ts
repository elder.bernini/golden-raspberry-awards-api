import TypeORMDataSource from "@shared/datasource/sqlite/typeOrmDatasource";
import { inject, injectable } from "tsyringe";
import { Repository } from "typeorm";
import CreateStudioDto from "../dto/createStudio.dto";
import Studio from "../models/studio.model";
import StudioRepository from "../repositories/studio.repository";

@injectable()
class StudioRepositoryImpl implements StudioRepository {
  private repository: Repository<Studio>;

  constructor(
    @inject("TypeORMDataSource")
    typeORMDataSource: TypeORMDataSource
  ) {
    this.repository = typeORMDataSource.getRepository(Studio);
  }

  async save(data: CreateStudioDto): Promise<Studio> {
    const item = this.repository.create(data);

    await this.repository.save(item);

    return item;
  }

  findByName(name: string): Promise<Studio | null> {
    return this.repository.findOneBy({ name });
  }
}

export default StudioRepositoryImpl;
