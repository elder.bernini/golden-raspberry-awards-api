import TypeORMDataSource from "@shared/datasource/sqlite/typeOrmDatasource";
import { inject, injectable } from "tsyringe";
import { Repository } from "typeorm";
import CreateMovieDto from "../dto/createMovie.dto";
import Movie from "../models/movie.model";
import MovieRepository from "../repositories/movie.repository";

@injectable()
class MovieRepositoryImpl implements MovieRepository {
  private repository: Repository<Movie>;

  constructor(
    @inject("TypeORMDataSource")
    typeORMDataSource: TypeORMDataSource
  ) {
    this.repository = typeORMDataSource.getRepository(Movie);
  }

  async save(data: CreateMovieDto): Promise<Movie> {
    const item = this.repository.create(data);

    await this.repository.save(item);

    return item;
  }
}

export default MovieRepositoryImpl;
