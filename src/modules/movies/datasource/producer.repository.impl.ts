import TypeORMDataSource from "@shared/datasource/sqlite/typeOrmDatasource";
import { inject, injectable } from "tsyringe";
import { Repository } from "typeorm";
import CreateProducerDto from "../dto/createProducer.dto";
import Producer from "../models/producer.model";
import ProducerRepository from "../repositories/producer.repository";

@injectable()
class ProducerRepositoryImpl implements ProducerRepository {
  private repository: Repository<Producer>;

  constructor(
    @inject("TypeORMDataSource")
    typeORMDataSource: TypeORMDataSource
  ) {
    this.repository = typeORMDataSource.getRepository(Producer);
  }
  findWinners(): Promise<Producer[]> {
    throw new Error("Method not implemented.");
  }

  async save(data: CreateProducerDto): Promise<Producer> {
    const item = this.repository.create(data);

    await this.repository.save(item);

    return item;
  }

  findByName(name: string): Promise<Producer | null> {
    return this.repository.findOneBy({ name });
  }

  findWinnersMoreThanOnce(): Promise<Producer[]> {
    return this.repository
      .createQueryBuilder("producer")
      .innerJoinAndSelect("movie-producer", "mp", "producer.id = mp.producerId")
      .innerJoinAndSelect(
        "producer.movies",
        "movie",
        "mp.movieId = movie.id and movie.win = 1"
      )
      .where((qb) => {
        const sub = qb
          .subQuery()
          .select("p.id")
          .from(Producer, "p")
          .innerJoin("movie-producer", "mp", "p.id = mp.producerId")
          .innerJoin("p.movies", "m", "mp.movieId = m.id and m.win = 1")
          .groupBy("p.id")
          .having("count(*) > 1")
          .getQuery();

        return "producer.id IN " + sub;
      })
      .getMany();
  }
}

export default ProducerRepositoryImpl;
