import "reflect-metadata";
import request from "supertest";

import app from "@shared/app";
import datasourceInitializer from "@shared/datasource/datasourceInitializer";

import loadMoviesOnInitService from "@modules/movies/services/loadMoviesOnInit.service";

describe("Movies integration test", () => {
  beforeAll(async () => {
    await datasourceInitializer.init();
    await loadMoviesOnInitService.execute();
  }, 100000);

  it("should return the expected values with default parameters", async () => {
    const res = await request(app).get(
      "/api/movies/producers/min-max-interval"
    );

    expect(200).toBe(res.status);
    expect(1).toBe(res.body.min.length);
    expect(6).toBe(res.body.min[0].interval);
    expect(1).toBe(res.body.max.length);
    expect(13).toBe(res.body.max[0].interval);
  }, 100000);
});
